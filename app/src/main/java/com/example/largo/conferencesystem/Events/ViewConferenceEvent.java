package com.example.largo.conferencesystem.Events;

import Api.Conferences;

/**
 * Created by Largo on 25.04.2017.
 */
public class ViewConferenceEvent {

    public Conferences conference;

    public ViewConferenceEvent(Conferences conference) {
        this.conference = conference;
    }
}
