package com.example.largo.conferencesystem;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.register_user)
    EditText registerUser;
    @BindView(R.id.register_password)
    EditText registerPassword;
    @BindView(R.id.register_email)
    EditText registerEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
    }

    public void register(View view) {

        Log.e("REGISTER", registerUser.getText().toString());
        final String user = registerUser.getText().toString();
        final String pass = registerPassword.getText().toString();
        final String email = registerEmail.getText().toString();

        if (user.isEmpty())
            registerUser.setError(getString(R.string.fill_field));
        else if (pass.isEmpty())
            registerPassword.setError(getString(R.string.fill_field));
        else if (email.isEmpty())
            registerEmail.setError(getString(R.string.fill_field));
        else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
            registerEmail.setError(getString(R.string.bad_format_email));
        else {
            Auth auth = new Auth();
            auth.setUser(user);
            auth.setPassword(pass);
            auth.setEmail(email);
            auth.registerUser();

            Toast.makeText(getApplicationContext(), R.string.msg_activate, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }
}
