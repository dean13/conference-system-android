package com.example.largo.conferencesystem;

import android.util.Log;

import java.io.IOException;

import Api.BodyContent;
import Api.Login;
import Api.Rest;
import Api.RestAPI;
import Api.RestCallback;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Largo on 21.02.2017.
 */

public class Auth{

    private String user;
    private String password;
    private String email;
    private Retrofit retrofit;

    private static final String ROOT_URL = "http://your-service.pl/";

    Auth() {

        buildAdapter();
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    void loginUser(final RestCallback restCallback) {

        BodyContent bodyContent = new BodyContent();
        bodyContent.setLogin(user);
        bodyContent.setPassword(password);


        Call<Login> response = Rest.get().postLogin(bodyContent);
        response.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                restCallback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {

            }
        });
    }

    void registerUser() {

        BodyContent bodyContent = new BodyContent();
        bodyContent.setLogin(user);
        bodyContent.setPassword(password);
        bodyContent.setEmail(email);

        Call<ResponseBody> response = Rest.get().postRegister(bodyContent);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    void restorePassword() {

        BodyContent bodyContent = new BodyContent();
        bodyContent.setEmail(email);

        Call<ResponseBody> response = Rest.get().postEmail(bodyContent);
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void buildAdapter() {

        retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    /*public interface ResponseCallback {
        void status(String stat);
    }*/

}
