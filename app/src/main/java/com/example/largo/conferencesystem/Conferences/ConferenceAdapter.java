package com.example.largo.conferencesystem.Conferences;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.largo.conferencesystem.Events.GoConferenceEvent;
import com.example.largo.conferencesystem.R;
import com.squareup.otto.Bus;

import java.util.ArrayList;
import java.util.List;

import Api.Conferences;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConferenceAdapter extends RecyclerView.Adapter<ConferenceViewHolder> {

    private List<Conferences> conferences = new ArrayList<>();
    private Bus bus;

    public  ConferenceAdapter(Bus bus) {

        this.bus = bus;
    }

    @Override
    public ConferenceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new ConferenceViewHolder(layoutInflater.inflate(R.layout.conference_item, parent, false), bus);
    }

    @Override
    public void onBindViewHolder(ConferenceViewHolder holder, int position) {

        holder.setConferences(conferences.get(position));
    }

    @Override
    public int getItemCount() {
        return conferences.size();
    }


    public void setConference(List<Conferences> results) {
        conferences.clear();
        conferences.addAll(results);
        notifyDataSetChanged();
    }
}

class ConferenceViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.conferenceTitle)
    TextView conferenceTitle;
    @BindView(R.id.conferenceCity)
    TextView conferenceCity;
    @BindView(R.id.conferenceGo)
    ImageButton conferenceGo;

    private final Bus bus;
    private Conferences conferences;

    ConferenceViewHolder(View itemView, Bus bus) {
        super(itemView);
        this.bus = bus;
        ButterKnife.bind(this, itemView);
    }

    public void setConferences(Conferences conferences) {
        this.conferences = conferences;
        conferenceTitle.setText(conferences.title);
        conferenceCity.setText(conferences.town);
    }

    @OnClick(R.id.conferenceGo)
    public void goConference() {

        bus.post(new GoConferenceEvent(conferences));
    }
}
