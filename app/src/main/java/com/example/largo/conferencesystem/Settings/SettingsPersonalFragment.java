package com.example.largo.conferencesystem.Settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;

import com.example.largo.conferencesystem.AlertActivity;
import com.example.largo.conferencesystem.App;
import com.example.largo.conferencesystem.DashboardActivity;
import com.example.largo.conferencesystem.R;
import com.example.largo.conferencesystem.SessionManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import Api.Person;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.ResponseBody;


public class SettingsPersonalFragment extends Fragment implements DatePicker.OnDateChangedListener {

    @BindView(R.id.personalName)
    EditText personalName;
    @BindView(R.id.personalSurname)
    EditText personalSurname;
    @BindView(R.id.personalBirth)
    EditText personalBirth;
    @BindView(R.id.personalPhone)
    EditText personalPhone;
    @BindView(R.id.personalHome)
    EditText personalHome;
    @BindView(R.id.personalPost)
    EditText personalPost;
    @BindView(R.id.personalTown)
    EditText personalTown;
    @BindView(R.id.personalCountry)
    EditText personalCountry;
    @BindView(R.id.fabPersonal)
    FloatingActionButton fabPersonal;
    @BindView(R.id.personalStreet)
    EditText personalStreet;

    Unbinder unbinder;
    Calendar calendar;
    Person person;

    private SettingsManager settingsManager;
    private AlertActivity alertActivity;
    private SimpleDateFormat sdf;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personal_settings, container, false);
        unbinder = ButterKnife.bind(this, view);
        person = new Person();
        calendar = Calendar.getInstance();

        sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        personalBirth.setText(sdf.format(calendar.getTime()));

        //personalBirth.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), this);
        person.birth_day = calendar.getTime();

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App app = ((App) getActivity().getApplication());
        settingsManager = app.getSettingsManager();
        alertActivity = app.getAlertActivity();

        settingsManager.getPerson(Integer.parseInt(settingsManager.getUser().get(SessionManager.KEY_ID)));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        settingsManager.onStartPersonal(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        settingsManager.onStopPersonal();
    }

    @OnClick(R.id.fabPersonal)
    public void savePersonal() {

        person.name = personalName.getText().toString();
        person.lastname = personalSurname.getText().toString();
        person.phone = personalPhone.getText().toString();
        person.home = personalHome.getText().toString();
        person.postcode = personalPost.getText().toString();
        person.street = personalStreet.getText().toString();
        person.town = personalTown.getText().toString();
        person.country = personalCountry.getText().toString();
        person.id_user = Integer.parseInt(settingsManager.getUser().get(SessionManager.KEY_ID));

        if (person.name.isEmpty())
            personalName.setError(getString(R.string.fill_field));
        else if (person.lastname.isEmpty())
            personalSurname.setError(getString(R.string.fill_field));
        else if (person.phone.isEmpty())
            personalPhone.setError(getString(R.string.fill_field));
        else if (!Patterns.PHONE.matcher(person.phone).matches() || person.phone.length() < 9)
            personalPhone.setError(getString(R.string.bad_format_phone));
        else if (person.home.isEmpty())
            personalHome.setError(getString(R.string.fill_field));
        else if (person.postcode.isEmpty())
            personalPost.setError(getString(R.string.fill_field));
        else if (person.street.isEmpty())
            personalStreet.setError(getString(R.string.fill_field));
        else if (person.town.isEmpty())
            personalTown.setError(getString(R.string.fill_field));
        else if (person.country.isEmpty())
            personalCountry.setError(getString(R.string.fill_field));
        else
            settingsManager.savePersonal(person);
    }

    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();

        if (R.id.start == view.getId()) {
            calendar.set(year, monthOfYear, dayOfMonth);
            person.birth_day = calendar.getTime();
        }
    }

    public void savedPerson() {

        Intent intent = new Intent(getContext(), DashboardActivity.class);
        startActivity(intent);

        alertActivity.showToast(getString(R.string.msg_update_person));
    }

    public void showPersonConfig(Person person) {

        calendar.setTime(person.birth_day);

        /*int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);*/

        sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

        personalName.setText(person.name);
        personalSurname.setText(person.lastname);
        personalBirth.setText(sdf.format(calendar.getTime()));
        personalPhone.setText(person.phone);
        personalHome.setText(person.home);
        personalStreet.setText(person.street);
        personalPost.setText(person.postcode);
        personalTown.setText(person.town);
        personalCountry.setText(person.country);
    }
}
