package com.example.largo.conferencesystem.Submissions;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageButton;

/**
 * Created by Largo on 01.06.2017.
 */

public class SubmissionActionButton extends ImageButton {

    public SubmissionActionButton(Context context) {
        super(context);
    }


    public SubmissionActionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SubmissionActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
