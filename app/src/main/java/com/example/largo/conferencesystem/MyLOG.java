package com.example.largo.conferencesystem;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class MyLOG extends AppCompatActivity {

    private static String TAG;

    MyLOG() {
        TAG = getClass().getSimpleName();
    }

    public static void e(String msg) {
        Log.e(TAG, msg);
    }
}
