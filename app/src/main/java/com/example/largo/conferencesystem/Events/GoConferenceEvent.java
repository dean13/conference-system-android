package com.example.largo.conferencesystem.Events;

import Api.Conferences;

public class GoConferenceEvent {
    public Conferences conferences;

    public GoConferenceEvent(Conferences conferences) {
        this.conferences = conferences;
    }
}
