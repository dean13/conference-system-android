package com.example.largo.conferencesystem.Conferences;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.largo.conferencesystem.App;
import com.example.largo.conferencesystem.Events.GoConferenceEvent;
import com.example.largo.conferencesystem.Events.ViewConferenceEvent;
import com.example.largo.conferencesystem.MyLOG;
import com.example.largo.conferencesystem.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import Api.Conferences;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ConferenceFragment extends Fragment{

    @BindView(R.id.titleConference)
    TextView title;
    @BindView(R.id.descriptionConference)
    TextView description;
    @BindView(R.id.organizer)
    TextView organizer;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.start)
    TextView start;
    @BindView(R.id.end)
    TextView end;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.website)
    TextView website;
    @BindView(R.id.categoriesImage)
    ImageView categoriesImage;
    @BindView(R.id.categories)
    TextView categories;
    @BindView(R.id.mapConference)
    MapView mapView;

    GoogleMap map;
    Unbinder unbinder;
    CameraUpdate cameraUpdate;
    Conferences conference;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String conferenceJSON = this.getArguments().getString("CONFERENCE");
        conference = new Gson().fromJson(conferenceJSON, Conferences.class);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_conference, container, false);
        unbinder = ButterKnife.bind(this, view);

        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity())) {
            case ConnectionResult.SUCCESS:
                Toast.makeText(getActivity(), "SUCCESS", Toast.LENGTH_SHORT).show();
                mapView.onCreate(savedInstanceState);
                // Gets to GoogleMap from the MapView and does initialization stuff
                if (mapView != null) {
                    mapView.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            map = googleMap;

                            // Add a marker in Sydney and move the camera

                            LatLng address = getLocationFromAddress(conference.town + ", " + conference.country);
                            map.addMarker(new MarkerOptions().position(address).title(conference.title));
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(address, 15));
                            //map.animateCamera(CameraUpdateFactory.zoomIn());
                            map.getUiSettings().setZoomControlsEnabled(true);
                        }
                    });
                }
                break;
            case ConnectionResult.SERVICE_MISSING:
                Toast.makeText(getActivity(), "GOOGLE PLAY SERVICE MISSING", Toast.LENGTH_SHORT).show();
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                Toast.makeText(getActivity(), "GOOGLE PLAY UPDATE REQUIRED", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(getActivity(), GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()), Toast.LENGTH_SHORT).show();
        }

        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, new Locale("pl","PL"));
        String formattedBegin = dateFormat.format(conference.begin);
        String formattedEnd = dateFormat.format(conference.end);

        title.setText(conference.title);
        description.setText(conference.description);
        organizer.append(String.valueOf(conference.id_organizator));
        phone.append(conference.phone);
        email.append(conference.mail);
        start.append(formattedBegin);
        end.append(formattedEnd);
        address.append(conference.street + ", " + conference.town);
        website.append(conference.website);

        categories.setText(conference.category);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public LatLng getLocationFromAddress(String strAddress) {

        Geocoder coder = new Geocoder(getContext(), Locale.getDefault());
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());

            return p1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return p1;
    }

}
