package com.example.largo.conferencesystem.Events;

import Api.Submissions;


public class GoSubmissionEvent {
    public Submissions submissions;
    public int id;
    public int id_user;

    public GoSubmissionEvent(Submissions submissions) {
        this.submissions = submissions;
    }

    public GoSubmissionEvent(int id, int id_user) {

        this.id = id;
        this.id_user = id_user;
    }
}