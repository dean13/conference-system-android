package com.example.largo.conferencesystem;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RestoreActivity extends AppCompatActivity {

    @BindView(R.id.restore_text_email)
    EditText restoreTextEmail;
    @BindView(R.id.restore_button)
    Button restoreButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restore);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.restore_button)
    public void restore() {

        Auth auth = new Auth();
        auth.setEmail(restoreTextEmail.getText().toString());
        auth.restorePassword();

        Intent dialogIntent = new Intent(getApplicationContext(), AlertActivity.class);
        dialogIntent.putExtra("EXTRA_MSG", getString(R.string.msg_sent_email));
        startActivity(dialogIntent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }
}
