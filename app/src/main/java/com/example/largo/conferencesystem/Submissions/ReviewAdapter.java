package com.example.largo.conferencesystem.Submissions;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.largo.conferencesystem.R;

import java.util.ArrayList;
import java.util.List;

import Api.Reviews;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Largo on 02.06.2017.
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewViewHolder> {

    private List<Reviews> reviews = new ArrayList<>();
    private String user;

    public ReviewAdapter(String user) {

        this.user = user;
    }

    @Override
    public ReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new ReviewViewHolder(layoutInflater.inflate(R.layout.review_item, parent, false), user);
    }

    @Override
    public void onBindViewHolder(ReviewViewHolder holder, int position) {

        holder.setReview(reviews.get(position));
    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }


    public void setReviews(List<Reviews> results) {
        reviews.clear();
        reviews.addAll(results);
        notifyDataSetChanged();
    }
}

class ReviewViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.review_opinion)
    TextView reviewOpinion;
    @BindView(R.id.review_author)
    TextView reviewAuthor;
    @BindView(R.id.review_date)
    TextView reviewDate;
    @BindView(R.id.review_rate)
    TextView reviewRate;
    @BindView(R.id.review_experience)
    TextView reviewExperience;
    private Reviews review;
    private String user;

    ReviewViewHolder(View itemView, String user) {
        super(itemView);
        this.user = user;
        ButterKnife.bind(this, itemView);
    }

    public void setReview(Reviews review) {

        this.review = review;
        reviewOpinion.setText(review.opinion);
        reviewAuthor.setText(user);
        reviewDate.setText(review.getDate());
        reviewRate.setText(review.getRate());
        reviewExperience.setText(review.getExperience());
    }
}
