package com.example.largo.conferencesystem;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.largo.conferencesystem.Conferences.CreateConferenceFragment;
import com.example.largo.conferencesystem.Conferences.DashboardConferenceFragment;
import com.example.largo.conferencesystem.Settings.SettingsActivity;
import com.example.largo.conferencesystem.Submissions.SubmissionCreateFragment;
import com.example.largo.conferencesystem.Submissions.SubmissionDashboardFragment;

import java.util.HashMap;

import Api.Conferences;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, CreateConferenceFragment.OnSaveConference{

    /*@BindView(R.id.fab)
    FloatingActionButton fab;*/
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    /*@Nullable @BindView(R.id.nav_view)
    NavigationView navView;*/
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    NavigationView navigationView;

    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_drawer);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        TextView drawerNavName = (TextView) headerView.findViewById(R.id.drawerNavName);

        sessionManager = new SessionManager(getApplicationContext());


        /*TextView lblUser = (TextView) findViewById(R.id.lblUser);
        TextView lblPassword = (TextView) findViewById(R.id.lblPassword);*/
        //btnLogout = (Button) findViewById(R.id.btnLogout);

        //Toast.makeText(getApplicationContext(), "User status " + sessionManager.isLoggedIn(), Toast.LENGTH_LONG).show();

        //sessionManager.checkLogin();
        HashMap<String, String> user = sessionManager.getUserDetails();
        String name = user.get(SessionManager.KEY_NAME);

        drawerNavName.setText(name);
        /*lblUser.setText(name);
        lblPassword.setText(password);*/

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);
            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_conference) {

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_dashboard_drawer, new DashboardConferenceFragment()).addToBackStack(null).commit();

        } else if (id == R.id.nav_logout) {
            sessionManager.logoutUser();
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void goToConferences() {
        MenuItem item = navigationView.getMenu().findItem(R.id.nav_conference);
        item.setChecked(true);
        onNavigationItemSelected(item);

    }

}
