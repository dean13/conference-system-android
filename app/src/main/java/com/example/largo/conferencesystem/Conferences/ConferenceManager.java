package com.example.largo.conferencesystem.Conferences;

import android.util.Log;

import com.example.largo.conferencesystem.Events.GoConferenceEvent;
import com.example.largo.conferencesystem.SessionManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Api.ConferenceResponse;
import Api.Conferences;
import Api.Rest;
import Api.RestAPI;
import Api.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ConferenceManager{

    private final RestAPI restApi;
    private final Bus bus;
    private final HashMap<String, String> user;
    private Call<ConferenceResponse> call;

    private CreateConferenceFragment createConferenceFragment;
    private DashboardConferenceFragment conferenceFragment;

    public ConferenceManager(Rest rest, Bus bus, SessionManager sessionManager) {
        restApi = rest.get();
        this.bus = bus;
        bus.register(this);
        user = sessionManager.getUserDetails();
    }

    interface OnGetUsers{
        void onShowUsers(List<User> body);
    }

    void onStartDashboard(DashboardConferenceFragment fragment) {
        conferenceFragment = fragment;
    }

    void onStopDashboard() {
        conferenceFragment = null;
    }

    void onStartCreate(CreateConferenceFragment fragment) {
        createConferenceFragment = fragment;
    }

    void onStopCreate() {
        createConferenceFragment = null;
    }

    void loadConferences() {
        call = restApi.getConferences();
        call.enqueue(new Callback<ConferenceResponse>() {
            @Override
            public void onResponse(Call<ConferenceResponse> call, Response<ConferenceResponse> response) {
                if (response.isSuccessful()) {

                    for (Conferences result : response.body().results) {
                        Log.d(ConferenceManager.class.getSimpleName(), "Conference: " + result);
                    }

                    if (conferenceFragment != null) {
                        conferenceFragment.showConferences(response.body().results);
                    }
                }
            }

            @Override
            public void onFailure(Call<ConferenceResponse> call, Throwable t) {
                Log.e("fail", t.getMessage());
            }
        });
    }

    void addConference(final Conferences conferences) {
        conferences.id_organizator = Integer.parseInt(user.get(SessionManager.KEY_ID));
        call = restApi.storeConference(conferences);
        call.enqueue(new Callback<ConferenceResponse>() {
            @Override
            public void onResponse(Call<ConferenceResponse> call, Response<ConferenceResponse> response) {
                if (response.isSuccessful()) {

                    if (createConferenceFragment != null) {
                        createConferenceFragment.createdConference();
                    }
                }
            }

            @Override
            public void onFailure(Call<ConferenceResponse> call, Throwable t) {
                Log.e("fail", t.getMessage());
            }
        });
    }

    @Subscribe
    public void onGoConference(GoConferenceEvent event) {
        Call<Conferences> callInfo = restApi.showConference(event.conferences.id, Integer.parseInt(user.get(SessionManager.KEY_ID)));
        callInfo.enqueue(new Callback<Conferences>() {
            @Override
            public void onResponse(Call<Conferences> call, Response<Conferences> response) {
                if (response.isSuccessful()) {

                    if (conferenceFragment != null) {
                        conferenceFragment.showConference(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<Conferences> call, Throwable t) {
                Log.e(getClass().getSimpleName(), t.getMessage());
            }
        });
    }

    public void getCommittee(final OnGetUsers onGetUsers) {
        Call<List<User>> callBody = restApi.getUsers();
        callBody.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.isSuccessful()) {

                    if (createConferenceFragment != null) {
                        onGetUsers.onShowUsers(response.body());
                        //createConferenceFragment.showCommittee(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.e(getClass().getSimpleName(), t.getMessage());
            }
        });
    }
}

