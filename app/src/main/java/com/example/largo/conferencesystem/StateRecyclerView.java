package com.example.largo.conferencesystem;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;


public class StateRecyclerView extends RecyclerView {

    private View loadingView;
    private View emptyView;

    private AdapterDataObserver dataObserver = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            Adapter adapter = getAdapter();
            boolean hasData = adapter.getItemCount() > 0;
            setVisibility(hasData ? VISIBLE : GONE);
            emptyView.setVisibility(hasData ? GONE : VISIBLE);
        }
    };

    public StateRecyclerView(Context context) {
        super(context);
    }

    public StateRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public StateRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setAdapter(Adapter adapter) {
        super.setAdapter(adapter);

        loadingView.setVisibility(adapter == null ? VISIBLE : GONE);
        if (adapter != null) {
            adapter.registerAdapterDataObserver(dataObserver);
            dataObserver.onChanged();
        }
    }

    public void setLoadingView(View loadingView) {
        this.loadingView = loadingView;
    }

    public void setEmptyView(View emptyView) {
        this.emptyView = emptyView;
    }
}
