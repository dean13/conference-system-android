package com.example.largo.conferencesystem.Conferences;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.largo.conferencesystem.App;
import com.example.largo.conferencesystem.R;
import com.example.largo.conferencesystem.StateRecyclerView;
import com.google.gson.Gson;
import com.squareup.otto.Bus;

import java.util.List;

import Api.Conferences;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class DashboardConferenceFragment extends Fragment {

    @BindView(R.id.conferenceRecycler)
    StateRecyclerView conferenceRecycler;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    Unbinder unbinder;
    @BindView(R.id.progressBarConference)
    ProgressBar progressBarConference;
    @BindView(R.id.emptyLayoutConference)
    LinearLayout emptyLayoutConference;
    //Unbinder unbinder;

    private ConferenceManager conferenceManager;
    private Bus bus;
    //public OnViewConference onViewConference;

    /*interface OnViewConference {
        void responseConference(Conferences response);
    }*/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App app = ((App) getActivity().getApplication());
        conferenceManager = app.getConferenceManager();
        bus = app.getBus();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(getClass().getSimpleName(), "onCreateView");
        View view = inflater.inflate(R.layout.fragment_dashboard_conference, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        conferenceRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        conferenceRecycler.setEmptyView(emptyLayoutConference);
        conferenceRecycler.setLoadingView(progressBarConference);
        Log.d(getClass().getSimpleName(), "onViewCreated");
    }

    @Override
    public void onStart() {
        super.onStart();
        conferenceManager.onStartDashboard(this);
        conferenceManager.loadConferences();
        //bus.register(this);
        Log.d(getClass().getSimpleName(), "onStart");
    }

    @Override
    public void onStop() {
        super.onStop();
        conferenceManager.onStopDashboard();
        //bus.unregister(this);
        Log.d(getClass().getSimpleName(), "onStop");
    }

    public void showConferences(List<Conferences> results) {
        Log.d(getClass().getSimpleName(), "showConferences");
        ConferenceAdapter adapter = new ConferenceAdapter(bus);
        adapter.setConference(results);
        conferenceRecycler.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //unbinder.unbind();
        unbinder.unbind();
    }

    @OnClick(R.id.fab)
    public void createConference() {

        getFragmentManager().beginTransaction()
                .replace(R.id.content_dashboard_drawer, new CreateConferenceFragment()).addToBackStack(null).commit();
    }

    public void showConference(final Conferences conference) {

        //ConferenceFragment conferenceFragment = new ConferenceFragment();
        //onViewConference.responseConference(results);
        //conferenceFragment.setConferenceData(conference);

        ConferenceActivity conferenceActivity = new ConferenceActivity();
        Intent intent = new Intent(getContext(), conferenceActivity.getClass());
        intent.putExtra("CONFERENCE", new Gson().toJson(conference));
        startActivity(intent);

        //conferenceActivity.setConferenceData(conference);


        /*getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_dashboard_drawer, conferenceFragment)
                .addToBackStack(null)
                .commit();*/

    }
}
