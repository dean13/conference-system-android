package com.example.largo.conferencesystem.Submissions;

import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.largo.conferencesystem.Events.GoDecisionDialog;
import com.example.largo.conferencesystem.Events.GoSubmissionEvent;
import com.example.largo.conferencesystem.R;
import com.squareup.otto.Bus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import Api.Submissions;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubmissionAdapter extends RecyclerView.Adapter<SubmissionViewHolder> {

    @BindView(R.id.submissionAction)
    ImageButton submissionAction;
    @BindView(R.id.submissionTitle)
    TextView submissionTitle;
    private List<Submissions> submissions = new ArrayList<>();
    private Bus bus;
    private int user;

    public SubmissionAdapter(Bus bus, int user) {
        this.bus = bus;
        this.user = user;
    }

    @Override
    public SubmissionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new SubmissionViewHolder(layoutInflater.inflate(R.layout.submission_item, parent, false), bus, user);
    }

    @Override
    public void onBindViewHolder(SubmissionViewHolder holder, int position) {

        holder.setSubmission(submissions.get(position));
    }

    @Override
    public int getItemCount() {
        return submissions.size();
    }


    public void setSubmission(List<Submissions> results) {
        submissions.clear();
        submissions.addAll(results);
        notifyDataSetChanged();
    }

}

class SubmissionViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.submissionTitle)
    TextView submissionTitle;
    @BindView(R.id.submissionAuthor)
    TextView submissionAuthor;
    @BindView(R.id.submissionDeadline)
    TextView submissionDeadline;
    @BindView(R.id.submissionAction)
    ImageButton submissionAction;

    private final Bus bus;
    private int user;
    private Submissions submissions;
    private Date now;

    SubmissionViewHolder(View itemView, Bus bus, int user) {
        super(itemView);
        this.bus = bus;
        this.user = user;
        ButterKnife.bind(this, itemView);
        now = new Date();
    }

    public void setSubmission(Submissions submissions) {
        this.submissions = submissions;
        submissionTitle.setText(submissions.title);
        submissionAuthor.setText(submissions.authors);
        submissionDeadline.setText(submissions.getDate());

        if (submissions.deadline.before(now) && submissions.decision == 0) {
            submissionAction.setImageResource(R.drawable.ic_submission_ready);
        }
        else if (submissions.decision != 0) {
            submissionAction.setClickable(false);

            if (submissions.decision == 1)
                submissionAction.setImageResource(R.drawable.ic_submission_approved);
            else
                submissionAction.setImageResource(R.drawable.ic_submission_denied);
        }
        else {
            submissionAction.setClickable(false);
            submissionAction.setEnabled(false);
            submissionAction.setImageResource(R.drawable.ic_submission_waiting);
        }
    }

    @OnClick(R.id.submissionTitle)
    public void onSubmissionTitle() {
        bus.post(new GoSubmissionEvent(submissions.id, user));
    }

    @OnClick(R.id.submissionAction)
    public void onSubmissionAction() {
        bus.post(new GoDecisionDialog(submissions.id));
    }
}
