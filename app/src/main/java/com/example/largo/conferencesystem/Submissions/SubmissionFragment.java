package com.example.largo.conferencesystem.Submissions;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.largo.conferencesystem.App;
import com.example.largo.conferencesystem.R;
import com.example.largo.conferencesystem.SessionManager;
import com.example.largo.conferencesystem.StateRecyclerView;

import org.apache.commons.io.FilenameUtils;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import Api.Reviews;
import Api.Submissions;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class SubmissionFragment extends Fragment implements AdapterView.OnItemSelectedListener, SubmissionManager.OnSaveReview {

    @BindView(R.id.submission_title)
    TextView submissionTitle;
    @BindView(R.id.submission_author)
    TextView submissionAuthor;
    @BindView(R.id.submission_organization)
    TextView submissionOrganization;
    @BindView(R.id.submission_description)
    TextView submissionDescription;
    Unbinder unbinder;
    Submissions submission;
    @BindView(R.id.submission_link)
    Button submissionLink;
    @BindView(R.id.opinion)
    EditText opinion;
    @BindView(R.id.rate)
    Spinner rate;
    @BindView(R.id.experience)
    Spinner experience;
    @BindView(R.id.review_save)
    Button reviewSave;

    Reviews review;
    @BindView(R.id.allowLayoutReview)
    LinearLayout allowLayoutReview;
    @BindView(R.id.deniedInfoReview)
    TextView deniedInfoReview;
    @BindView(R.id.deniedLayoutReview)
    LinearLayout deniedLayoutReview;
    /*@BindView(R.id.review_row_opinion)
    TextView reviewRowOpinion;
    @BindView(R.id.review_row_author)
    TextView reviewRowAuthor;
    @BindView(R.id.review_row_date)
    TextView reviewRowDate;
    @BindView(R.id.review_row_rate)
    TextView reviewRowRate;
    @BindView(R.id.review_row_experience)
    TextView reviewRowExperience;*/
    @BindView(R.id.reviewRecycler)
    StateRecyclerView reviewRecycler;
    @BindView(R.id.progressBarReviews)
    ProgressBar progressBarReviews;
    @BindView(R.id.emptyLayoutReviews)
    LinearLayout emptyLayoutReviews;
    private SubmissionManager submissionManager;
    private SubmissionManager.OnSaveReview onSaveReview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_submission, container, false);
        unbinder = ButterKnife.bind(this, view);

        ArrayAdapter<CharSequence> adapterRates = ArrayAdapter.createFromResource(getContext(),
                R.array.rates, android.R.layout.simple_spinner_item);
        adapterRates.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rate.setAdapter(adapterRates);
        rate.setOnItemSelectedListener(this);

        ArrayAdapter<CharSequence> adapterExperiences = ArrayAdapter.createFromResource(getContext(),
                R.array.experiences, android.R.layout.simple_spinner_item);
        adapterExperiences.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        experience.setAdapter(adapterExperiences);
        experience.setOnItemSelectedListener(this);

        allowToReview();

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        App app = ((App) getActivity().getApplication());
        submissionManager = app.getSubmissionManager();
        review = new Reviews();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        submissionTitle.append(" " + submission.title);
        submissionAuthor.append(submission.authors);
        submissionOrganization.append(submission.organization);
        submissionDescription.setText(submission.describe);

        reviewRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        reviewRecycler.setEmptyView(emptyLayoutReviews);
        reviewRecycler.setLoadingView(progressBarReviews);

        ReviewAdapter adapter = new ReviewAdapter(submissionManager.getUser().get(SessionManager.KEY_NAME));
        adapter.setReviews(submission.reviews);
        reviewRecycler.setAdapter(adapter);
    }

    public void setSubmission(Submissions submission) {
        this.submission = submission;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        submissionManager.onStartSubmission(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStop() {
        super.onStop();
        submissionManager.onStopSubmission();
    }

    @OnClick(R.id.submission_link)
    public void onDownloadArticle() {

        final File file = new File(submission.file_path);

        DownloadFiles(file);
    }

    private void DownloadFiles(File file_path) {
        //String extension = FilenameUtils.getExtension(file_path.getName());
        String fullPath = "http://your-service.pl" + file_path.getPath();
        String name = file_path.getName();

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(fullPath));
        //request.setDescription("Some descrition");
        request.setTitle(name);
        // in order for this if to run, you must use the android 3.2 to compile your app
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, name);

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);

        Toast.makeText(getContext(), "Plik został pobrany !", Toast.LENGTH_SHORT).show();
    }

    private void goToUrl(String url) {

        Uri web = Uri.parse(url);

        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            web = Uri.parse("http://" + url);
        }

        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, web);
        startActivity(launchBrowser);
    }

    @OnClick(R.id.review_save)
    public void onSaveReview() {

        review.id_user = 1;
        review.id_submission = submission.id;
        review.opinion = opinion.getText().toString();

        if (review.opinion.isEmpty())
            opinion.setError(getString(R.string.fill_field));
        else
            submissionManager.addReview(review, this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (R.id.rate == parent.getId()) {

            String ratingString = parent.getAdapter().getItem(position).toString();
            review.rate = Integer.parseInt(ratingString.substring(
                    ratingString.indexOf("(") + 1, ratingString.indexOf(")")));
        }
        if (R.id.experience == parent.getId()) {

            String reviewerString = parent.getAdapter().getItem(position).toString();
            review.experience = Integer.parseInt(reviewerString.substring(
                    reviewerString.indexOf("(") + 1, reviewerString.indexOf(")")));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onSavedReview() {
        Toast.makeText(getContext(), R.string.review_add_successfully, Toast.LENGTH_SHORT).show();
    }

    private void allowToReview() {

        Date date = new Date();

        if (submission.comittee.size() == 0) {
            allowLayoutReview.setVisibility(View.GONE);
            deniedLayoutReview.setVisibility(View.VISIBLE);
            deniedInfoReview.setText(R.string.review_error_commitee);
        } else if (submission.reviews.size() != 0) {
            allowLayoutReview.setVisibility(View.GONE);
            deniedLayoutReview.setVisibility(View.VISIBLE);
            deniedInfoReview.setText(R.string.review_error_added);
        } else if (date.after(submission.deadline)) {
            allowLayoutReview.setVisibility(View.GONE);
            deniedLayoutReview.setVisibility(View.VISIBLE);
            deniedInfoReview.setText(R.string.review_error_deadline);
        }
    }
}
