package com.example.largo.conferencesystem;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import Api.Login;
import Api.RestCallback;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    SessionManager sessionManager;

    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.sign_in_button)
    Button signInButton;
    private boolean doubleBackToExitPressedOnce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        sessionManager = new SessionManager(getApplicationContext());

        /*Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        startActivity(intent);
        finish();*/

    }

    public void registerIntent(View view) {

        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivity(intent);
        finish();
    }

    public void restoreIntent(View view) {

        Intent intent = new Intent(getApplicationContext(), RestoreActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.second_exit, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @OnClick(R.id.sign_in_button)
    public void loginClicked() {

        final String user = email.getText().toString();
        final String pass = password.getText().toString();

        if (isOnline()) {
            if (user.isEmpty())
                email.setError(getString(R.string.fill_field));
            else if (pass.isEmpty())
                password.setError(getString(R.string.fill_field));
            else {
                Auth loginAuth = new Auth();
                loginAuth.setUser(user);
                loginAuth.setPassword(pass);
                loginAuth.loginUser(new RestCallback() {
                    @Override
                    public void onSuccess(Login response) {
                        switch (response.STATUS) {
                            case "MSG_SUCCESS": {
                                sessionManager.createLoginSession(user, pass, response.ID);
                                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            }
                            case "MSG_ACTIVATE": {
                                Toast.makeText(getApplicationContext(), R.string.msg_activate, Toast.LENGTH_LONG).show();
                                break;
                            }
                            case "MSG_FAILED": {
                                Toast.makeText(getApplicationContext(), R.string.msg_failed, Toast.LENGTH_LONG).show();
                                break;
                            }

                        }
                    }
                });
            }
        }
        else {
            Toast.makeText(getApplicationContext(), "Brak połaczenia z internetem !", Toast.LENGTH_LONG).show();
        }

    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
