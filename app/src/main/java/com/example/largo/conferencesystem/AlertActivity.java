package com.example.largo.conferencesystem;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

public class AlertActivity {

    private Context context;

    AlertActivity(Context context) {

        this.context = context;
    }
    //private String EXTRA_MSG;

    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EXTRA_MSG = getIntent().getStringExtra("EXTRA_MSG");
        displayAlert();
    }*/

    public void showToast(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    /*private void displayAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(EXTRA_MSG).setCancelable(
                false).setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AlertActivity.this.finish();
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }*/

}
