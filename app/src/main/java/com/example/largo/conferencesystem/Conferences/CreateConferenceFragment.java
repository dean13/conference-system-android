package com.example.largo.conferencesystem.Conferences;


import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.largo.conferencesystem.AlertActivity;
import com.example.largo.conferencesystem.App;
import com.example.largo.conferencesystem.MultiSelectionSpinner;
import com.example.largo.conferencesystem.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import Api.Conferences;
import Api.User;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class CreateConferenceFragment extends Fragment implements
        AdapterView.OnItemSelectedListener,
        DatePicker.OnDateChangedListener, ConferenceManager.OnGetUsers {

    @BindView(R.id.title)
    EditText title;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.website)
    EditText website;
    @BindView(R.id.country_conference)
    EditText countryConference;
    @BindView(R.id.city)
    EditText city;
    @BindView(R.id.code)
    EditText code;
    @BindView(R.id.street)
    EditText street;
    @BindView(R.id.categories)
    Spinner categories;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.email)
    EditText email;
    Unbinder unbinder;
    @BindView(R.id.start_date)
    EditText startDate;
    @BindView(R.id.end_date)
    EditText endDate;
    @BindView(R.id.committee)
    MultiSelectionSpinner committee;

    private Context context;
    private OnSaveConference onSaveConference;
    private ConferenceManager conferenceManager;
    private AlertActivity alertActivity;
    private Conferences conferences;
    private Calendar calendar;
    private SimpleDateFormat sdf;
    public ArrayList<User> committeeArray;

    private String TAG = getClass().getSimpleName();

    public void createdConference() {
        onSaveConference.goToConferences();
        alertActivity.showToast(getString(R.string.create_conference));
    }

    @OnClick({R.id.start_date, R.id.end_date})
    public void onDateClicked(View view) {
        DatePickerDialog dialog;

        switch (view.getId()) {
            case R.id.start_date:
                dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        conferences.begin = calendar.getTime();
                        startDate.setText(sdf.format(calendar.getTime()));
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                dialog.show();

                break;
            case R.id.end_date:
                dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        conferences.end = calendar.getTime();
                        endDate.setText(sdf.format(calendar.getTime()));
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                dialog.show();
                break;
        }
    }

    @Override
    public void onShowUsers(List<User> users) {

        String[] arr = new String[users.size()];

        for (int i = 0; i < users.size(); i++) {
            arr[i] = users.get(i).login;
        }
        committee.setItems(arr);

    }

    public interface OnSaveConference {
        void goToConferences();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (R.id.categories == parent.getId()) {
            conferences.category = parent.getAdapter().getItem(position).toString();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();

        if (R.id.start == view.getId()) {
            calendar.set(year, monthOfYear, dayOfMonth);
            conferences.begin = calendar.getTime();
        } else if (R.id.end == view.getId()) {
            calendar.set(year, monthOfYear, dayOfMonth);
            conferences.end = calendar.getTime();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onSaveConference = (OnSaveConference) context;
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onSaveConference = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        conferenceManager.onStartCreate(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        conferenceManager.onStopCreate();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(getClass().getSimpleName(), "onCreate");
        setHasOptionsMenu(true);
        App app = ((App) getActivity().getApplication());
        conferenceManager = app.getConferenceManager();
        committeeArray = new ArrayList<>();
        conferenceManager.getCommittee(this);
        alertActivity = app.getAlertActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_conference, container, false);
        unbinder = ButterKnife.bind(this, view);
        conferences = new Conferences();
        calendar = Calendar.getInstance();

        ArrayAdapter<CharSequence> adapterCategories = ArrayAdapter.createFromResource(context,
                R.array.categories, android.R.layout.simple_spinner_item);
        adapterCategories.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categories.setAdapter(adapterCategories);
        categories.setOnItemSelectedListener(this);

        conferences.begin = calendar.getTime();
        conferences.end = calendar.getTime();

        sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        startDate.setText(sdf.format(calendar.getTime()));
        endDate.setText(sdf.format(calendar.getTime()));

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);




    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.conference, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_create: {
                getUserForm();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void getUserForm() {

        conferences.committee = committee.getSelectedStrings();
        conferences.title = title.getText().toString();
        conferences.description = description.getText().toString();
        conferences.website = website.getText().toString();
        conferences.country = countryConference.getText().toString();
        conferences.town = city.getText().toString();
        conferences.postcode = code.getText().toString();
        conferences.street = street.getText().toString();
        conferences.phone = phone.getText().toString();
        conferences.mail = email.getText().toString();

        if (conferences.title.isEmpty())
            title.setError(getString(R.string.fill_field));
        else if (conferences.country.isEmpty())
            countryConference.setError(getString(R.string.fill_field));
        else if (conferences.town.isEmpty())
            city.setError(getString(R.string.fill_field));
        else if (conferences.postcode.isEmpty())
            code.setError(getString(R.string.fill_field));
        else if (conferences.street.isEmpty())
            street.setError(getString(R.string.fill_field));
        else if (conferences.phone.isEmpty())
            phone.setError(getString(R.string.fill_field));
        else if (!Patterns.PHONE.matcher(conferences.phone).matches())
            phone.setError(getString(R.string.bad_format_phone));
        else if (conferences.mail.isEmpty())
            email.setError(getString(R.string.fill_field));
        else if (!Patterns.EMAIL_ADDRESS.matcher(conferences.mail).matches())
            email.setError(getString(R.string.bad_format_email));
        else
            conferenceManager.addConference(conferences);
    }

    public void showCommittee(ArrayList<User> committee) {

        committeeArray = committee;
    }
}
