package com.example.largo.conferencesystem;

import android.app.Application;

import com.example.largo.conferencesystem.Conferences.ConferenceManager;
import com.example.largo.conferencesystem.Settings.SettingsManager;
import com.example.largo.conferencesystem.Submissions.SubmissionManager;
import com.squareup.otto.Bus;

import Api.Rest;

public class App extends Application {

    private ConferenceManager conferenceManager;
    private SettingsManager settingsManager;

    private AlertActivity alertActivity;
    private Rest rest;
    private Bus bus;
    private SubmissionManager submissionManager;
    private SessionManager sessionManager;

    @Override
    public void onCreate() {
        super.onCreate();

        rest = new Rest();
        bus = new Bus();
        sessionManager = new SessionManager(getApplicationContext());
        conferenceManager = new ConferenceManager(rest, bus, sessionManager);
        submissionManager = new SubmissionManager(rest, bus, sessionManager);
        settingsManager = new SettingsManager(rest, sessionManager);
        alertActivity = new AlertActivity(getApplicationContext());
    }

    public ConferenceManager getConferenceManager() {
        return conferenceManager;
    }

    public Bus getBus() {
        return bus;
    }

    public AlertActivity getAlertActivity() {
        return alertActivity;
    }

    public SettingsManager getSettingsManager() {
        return settingsManager;
    }

    public SubmissionManager getSubmissionManager() {
        return submissionManager;
    }

    public SessionManager getSessionManager() {
        return sessionManager;
    }
}
