package com.example.largo.conferencesystem.Submissions;

import android.util.Log;

import com.example.largo.conferencesystem.Events.GoConferenceEvent;
import com.example.largo.conferencesystem.Events.GoDecisionDialog;
import com.example.largo.conferencesystem.Events.GoSubmissionEvent;
import com.example.largo.conferencesystem.SessionManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.util.HashMap;

import Api.Conferences;
import Api.Decision;
import Api.Rest;
import Api.RestAPI;
import Api.Reviews;
import Api.Submissions;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubmissionManager {

    private final RestAPI restApi;
    private final Bus bus;
    private final HashMap<String, String> user;
    private SubmissionDashboardFragment submissionFragment;
    private SubmissionCreateFragment createFragment;
    private SubmissionFragment submissionF;

    public SubmissionManager(Rest rest, Bus bus, SessionManager sessionManager) {
        restApi = rest.get();
        this.bus = bus;
        bus.register(this);
        user = sessionManager.getUserDetails();
    }

    public HashMap<String, String> getUser() {
        return user;
    }

    interface OnSaveReview{
        void onSavedReview();
    }

    interface OnSaveDecision{
        void onSavedDecision();
    }

    void onStartSubmission(SubmissionFragment submissionF) {
        this.submissionF = submissionF;
    }

    void onStopSubmission() {
        submissionF = null;
    }

    void onStartDashboard(SubmissionDashboardFragment submissionDashboardFragment) {
        submissionFragment = submissionDashboardFragment;
    }

    void onStopDashboard() {
        submissionFragment = null;
    }

    public void onStartCreate(SubmissionCreateFragment submissionCreateFragment) {
        createFragment = submissionCreateFragment;
    }

    public void onStopCreate() {
        createFragment = null;
    }

    public void addSubmission(File file, Submissions submission) {

        Log.e(getClass().getSimpleName(), String.valueOf(submission.deadline));

        RequestBody reqFile = RequestBody.create(MediaType.parse("*/*"), file);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), reqFile);
        RequestBody titlePart = RequestBody.create(okhttp3.MultipartBody.FORM, submission.title);
        RequestBody organizationPart = RequestBody.create(okhttp3.MultipartBody.FORM, submission.organization);
        RequestBody authorPart = RequestBody.create(okhttp3.MultipartBody.FORM, submission.author);
        RequestBody deadlinePart = RequestBody.create(okhttp3.MultipartBody.FORM, submission.deadline.toString());
        RequestBody userIDPart = RequestBody.create(okhttp3.MultipartBody.FORM, String.valueOf(submission.id_user));
        RequestBody describePart = RequestBody.create(okhttp3.MultipartBody.FORM, String.valueOf(submission.describe));

        Call<ResponseBody> fileCall = restApi.storeSubmission(
                filePart, submission.id_conference,
                titlePart, organizationPart, authorPart, deadlinePart, userIDPart, describePart
        );
        fileCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    createFragment.createdSubmission();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Subscribe
    public void onGoSubmission(GoSubmissionEvent event) {
        Call<Submissions> callInfo = restApi.showSubmission(event.id, event.id_user);
        callInfo.enqueue(new Callback<Submissions>() {
            @Override
            public void onResponse(Call<Submissions> call, Response<Submissions> response) {
                if (response.isSuccessful()) {

                    if (submissionFragment != null) {
                        submissionFragment.showSubmission(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<Submissions> call, Throwable t) {

            }
        });
    }

    @Subscribe
    public void goDecisionDialog(GoDecisionDialog event) {
        submissionFragment.showDecisionDialog(event.id);
    }

    public void addReview(Reviews review, final OnSaveReview onSaveReview) {
        Call<ResponseBody> callReview = restApi.storeReview(review, review.id_submission);
        callReview.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    if (submissionF != null) {
                        onSaveReview.onSavedReview();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("fail", t.getMessage());
            }
        });
    }

    public void saveDecision(int decision, int id_submission, final OnSaveDecision onSaveDecision) {

        Call<ResponseBody> callDecision = restApi.storeDecision(id_submission, decision);
        callDecision.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    if (submissionFragment != null) {
                        onSaveDecision.onSavedDecision();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("fail", t.getMessage());
            }
        });
    }
}
