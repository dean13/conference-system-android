package com.example.largo.conferencesystem.Settings;

import android.util.Log;

import com.example.largo.conferencesystem.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import Api.Conferences;
import Api.Person;
import Api.Rest;
import Api.RestAPI;
import Api.SettingsResponse;
import Api.User;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsManager {

    private final RestAPI restApi;

    private final HashMap<String, String> user;
    private SettingsPersonalFragment fragmentPersonal;
    private SettingsPasswordFragment fragmentPassword;

    public SettingsManager(Rest rest, SessionManager sessionManager) {
        restApi = rest.get();
        user = sessionManager.getUserDetails();
    }

    public HashMap<String, String> getUser() {
        return user;
    }

    interface OnChangePassword {
        void response(String error);
    }

    void onStartPersonal(SettingsPersonalFragment fragmentPersonal) {
        this.fragmentPersonal = fragmentPersonal;
    }
    void onStartPassword(SettingsPasswordFragment fragmentPassword) {
        this.fragmentPassword = fragmentPassword;
    }

    void onStopPersonal() {
        fragmentPersonal = null;
    }
    void onStopPassword() {
        fragmentPassword = null;
    }

    void savePersonal(Person person) {

        Call<ResponseBody> call = restApi.postPerson(person);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (fragmentPersonal != null) {
                    fragmentPersonal.savedPerson();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    void getPerson(int id) {

        Call<Person> call = restApi.getPerson(id);
        call.enqueue(new Callback<Person>() {
            @Override
            public void onResponse(Call<Person> call, Response<Person> response) {

                if (fragmentPersonal != null) {
                    fragmentPersonal.showPersonConfig(response.body());
                }
            }

            @Override
            public void onFailure(Call<Person> call, Throwable t) {

            }
        });
    }

    void changePassword(User user, final OnChangePassword onChangePassword) {

        Call<ResponseBody> call = restApi.changePassword(user);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (fragmentPassword != null) {

                    try {
                        JSONObject jObject = new JSONObject(response.body().string());
                        String error = jObject.getString("error");
                        String result = jObject.getString("result");

                        if (!error.isEmpty())
                            onChangePassword.response(error);
                        else if (!result.isEmpty())
                            fragmentPassword.changedPassword(result);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
