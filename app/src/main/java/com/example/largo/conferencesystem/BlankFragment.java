package com.example.largo.conferencesystem;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.largo.conferencesystem.Submissions.SubmissionDashboardFragment;

public class BlankFragment extends Fragment {

    String conferenceJSON;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        conferenceJSON = this.getArguments().getString("CONFERENCE");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_blank, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SubmissionDashboardFragment fragment = new SubmissionDashboardFragment();

        Bundle bundle = new Bundle();
        bundle.putString("CONFERENCE", conferenceJSON);
        fragment.setArguments(bundle);

        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_blank, fragment).commit();
    }
}
