package com.example.largo.conferencesystem.Submissions;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.largo.conferencesystem.App;
import com.example.largo.conferencesystem.R;
import com.example.largo.conferencesystem.SessionManager;
import com.example.largo.conferencesystem.StateRecyclerView;
import com.google.gson.Gson;
import com.squareup.otto.Bus;

import Api.Conferences;
import Api.Submissions;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubmissionDashboardFragment extends Fragment{


    Conferences conference;
    Unbinder unbinder;

    @BindView(R.id.progressBarSubmission)
    ProgressBar progressBarSubmission;
    @BindView(R.id.emptyLayoutSubmission)
    LinearLayout emptyLayoutSubmission;
    @BindView(R.id.submissionRecycler)
    StateRecyclerView submissionRecycler;
    @BindView(R.id.fabSubmission)
    FloatingActionButton fabSubmission;

    private SubmissionManager submissionManager;
    private OnCreateSubmission onCreateSubmission;
    private Bus bus;
    private String decisionSubmission;

    public SubmissionDashboardFragment() {
        // Required empty public constructor
    }

    public void showSubmission(Submissions submission) {

        SubmissionFragment fragment = new SubmissionFragment();

        getFragmentManager().beginTransaction().replace(R.id.fragment_blank, fragment)
                .addToBackStack(null).commit();

        fragment.setSubmission(submission);
    }

    public void showDecisionDialog(final int id_submission) {
        final CharSequence[] items = {getString(R.string.approved), getString(R.string.rejected)};

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle(getString(R.string.submission_set_decision));
        alertDialog.setNegativeButton(getString(R.string.zapisz),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        int decision = 0;

                        if (decisionSubmission != null) {
                            switch (decisionSubmission) {
                                case "Zatwierdzony": {
                                    decision = 1;
                                    break;
                                }
                                case "Odrzucony": {
                                    decision = 2;
                                    break;
                                }
                            }

                            submissionManager.saveDecision(decision, id_submission, new SubmissionManager.OnSaveDecision() {
                                @Override
                                public void onSavedDecision() {
                                    Toast.makeText(getContext(), R.string.decision_success, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        dialog.dismiss();
                    }
                });
        alertDialog.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                decisionSubmission = items[item].toString();
            }
        });
        alertDialog.create().show();
    }

    public interface OnCreateSubmission {
        void onCreateSubmission();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        App app = ((App) getActivity().getApplication());
        submissionManager = app.getSubmissionManager();
        bus = app.getBus();

        String conferenceJSON = this.getArguments().getString("CONFERENCE");
        conference = new Gson().fromJson(conferenceJSON, Conferences.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_submission_dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        submissionRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        submissionRecycler.setEmptyView(emptyLayoutSubmission);
        submissionRecycler.setLoadingView(progressBarSubmission);

        SubmissionAdapter adapter = new SubmissionAdapter(bus,
                Integer.parseInt(submissionManager.getUser().get(SessionManager.KEY_ID)));
        adapter.setSubmission(conference.submissions);
        submissionRecycler.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();
        submissionManager.onStartDashboard(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        submissionManager.onStopDashboard();
    }

    @OnClick(R.id.fabSubmission)
    public void onViewClicked() {

        SubmissionCreateFragment fragment = new SubmissionCreateFragment();

        Bundle bundle = new Bundle();
        bundle.putInt("ID_CONFERENCE", conference.id);
        fragment.setArguments(bundle);

        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_blank, fragment).commit();

    }
}
