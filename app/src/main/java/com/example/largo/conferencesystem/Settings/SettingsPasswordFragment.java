package com.example.largo.conferencesystem.Settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.largo.conferencesystem.AlertActivity;
import com.example.largo.conferencesystem.App;
import com.example.largo.conferencesystem.DashboardActivity;
import com.example.largo.conferencesystem.R;
import com.example.largo.conferencesystem.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import Api.User;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.ResponseBody;


public class SettingsPasswordFragment extends Fragment {

    @BindView(R.id.passwordOld)
    EditText passwordOld;
    @BindView(R.id.passwordNew)
    EditText passwordNew;
    @BindView(R.id.passwordConfirm)
    EditText passwordConfirm;
    @BindView(R.id.fabPassword)
    FloatingActionButton fabPassword;

    Unbinder unbinder;
    User user;

    private SettingsManager settingsManager;
    private AlertActivity alertActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_password_settings, container, false);
        unbinder = ButterKnife.bind(this, view);
        user = new User();

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App app = ((App)getActivity().getApplication());
        settingsManager = app.getSettingsManager();
        alertActivity = app.getAlertActivity();
    }

    @Override
    public void onStart() {
        super.onStart();
        settingsManager.onStartPassword(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        settingsManager.onStopPassword();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.fabPassword)
    public void changePassword() {

        user.id = Integer.parseInt(settingsManager.getUser().get(SessionManager.KEY_ID));
        user.actualPassword = passwordOld.getText().toString();
        user.password = passwordNew.getText().toString();
        user.password_confirmation = passwordConfirm.getText().toString();

        if (user.actualPassword.length() < 6)
            passwordOld.setError("Hasło za krótkie min 6");
        else if (user.password.length() < 6)
            passwordNew.setError("Hasło za krótkie min 6");
        else if (user.password_confirmation.length() < 6)
            passwordConfirm.setError("Hasło za krótkie min 6");
        else if (!user.password.equals(user.password_confirmation))
            passwordConfirm.setError("Hasła muszą być identyczne");
        else
            settingsManager.changePassword(user, new SettingsManager.OnChangePassword() {
                @Override
                public void response(String error) {
                    alertActivity.showToast(error);
                }
            });
    }

    public void changedPassword(String result) {

        Intent intent = new Intent(getContext(), DashboardActivity.class);
        startActivity(intent);

        alertActivity.showToast(result);
    }
}
