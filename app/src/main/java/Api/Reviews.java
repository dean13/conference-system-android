package Api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Largo on 01.06.2017.
 */

public class Reviews {

    public int id_user;
    public String opinion;
    public String attention;
    public Date date;
    public int experience;

    public int rate;
    public int id_submission;

    public int experience_reviewer;
    public int rating;

    public String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        return formatter.format(date);
    }

    public String getRate() {

        Map<Integer, String> rates = new HashMap<>();
        rates.put(3, "Mocna akceptacja");
        rates.put(2, "Akceptacja");
        rates.put(1, "Słaba akceptacja");
        rates.put(0, "Neutralna");
        rates.put(-1, "Słabe odrzucenie");
        rates.put(-2, "Odrzucenie");
        rates.put(-3, "Mocne odrzucenie");

        return rates.get(rating);
    }

    public String getExperience() {

        Map<Integer, String> experience = new HashMap<>();
        experience.put(5, "Ekspert");
        experience.put(4, "Doświadczony");
        experience.put(3, "Srednie");
        experience.put(2, "Niskie");
        experience.put(1, "Brak");

        return experience.get(experience_reviewer);
    }

}
