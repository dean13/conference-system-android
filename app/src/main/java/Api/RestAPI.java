package Api;

/*import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;*/

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Largo on 20.02.2017.
 */

public interface RestAPI {
    /*
    *auth query
    */
    @POST("/login")
    Call<Login> postLogin(@Body BodyContent bodyContent);

    @POST("/register")
    Call<ResponseBody> postRegister(@Body BodyContent bodyContent);

    @POST("/password/email")
    Call<ResponseBody> postEmail(@Body BodyContent bodyContent);

    @GET("/showUsers")
    Call<List<User>> getUsers();

    /*
    *conference query
    */
    @GET("/conferences")
    Call<ConferenceResponse> getConferences();

    @GET("conference/{id}/{user}")
    Call<Conferences> showConference(
            @Path("id") int conference,
            @Path("user") int user
    );

    @POST("/conference")
    Call<ConferenceResponse> storeConference(@Body Conferences conferences);

    /*
    *Settings query
    */
    @POST("/person")
    Call<ResponseBody> postPerson(@Body Person person);

    @POST("/personData/{id}")
    Call<Person> getPerson(@Path("id") int id);

    @POST("/changePassword")
    Call<ResponseBody> changePassword(@Body User user);

    /*
    * Submission query
    */
    /*@Multipart
    @POST("/submission/{id}")
    Call<ResponseBody> storeSubmission(
            @Body Submissions submission,
            @Path("id") int conference
    );*/

    @Multipart
    @POST("/submission/{id}")
    Call<ResponseBody> storeSubmission(
            @Part MultipartBody.Part file,
            @Path("id") int conference,
            @Part("title") RequestBody title,
            @Part("organization") RequestBody organization,
            @Part("author") RequestBody author,
            @Part("deadline") RequestBody deadline,
            @Part("id_user") RequestBody id_user,
            @Part("describe") RequestBody describe
    );

    @GET("submission/{id}/{user}")
    Call<Submissions> showSubmission(
            @Path("id") int submission,
            @Path("user") int user
    );

    /*reviews*/
    @POST("/review/{id}")
    Call<ResponseBody> storeReview(
            @Body Reviews review,
            @Path("id") int submission
    );

    /*decisions*/
    @POST("decision/{id}/{decision}")
    Call<ResponseBody> storeDecision(
            @Path("id") int submission,
            @Path("decision") int decision
    );
}
