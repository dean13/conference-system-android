package Api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Largo on 20.02.2017.
 */

public class BodyContent implements Serializable{

    private String login;
    private String password;
    private String email;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /*public BodyContent(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public BodyContent(String login, String password, String email) {
        this.login = login;
        this.password = password;
        this.email = email;
    }*/
}
