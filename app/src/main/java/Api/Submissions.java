package Api;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Submissions {

    public int id;
    public int id_user;
    public int id_conference;
    public int decision;

    public String title;
    public String organization;
    public String file;
    public String describe;
    public String author;
    public String authors;

    public Date date;
    public Date deadline;
    public String file_path;

    public List<Reviews> reviews;
    public List<Reviews> userReview;
    public List<Committee> comittee;

    public String message_warning;

    public String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        return formatter.format(date);
    }
}
