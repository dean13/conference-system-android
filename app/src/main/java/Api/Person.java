package Api;

import java.util.Date;

/**
 * Created by Largo on 24.04.2017.
 */

public class Person {

    public String name;
    public String lastname;
    public String phone;
    public String street;
    public String home;
    public String postcode;
    public String town;
    public String country;

    public Date birth_day;

    public int id_user;
}
