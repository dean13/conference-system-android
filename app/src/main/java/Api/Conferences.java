package Api;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Conferences {

    public String title;
    public String description;
    public String website;
    public String town;
    public String country;
    public String postcode;
    public String street;
    public String phone;
    public String mail;
    public String category;
    public String note;

    public Date begin;
    public Date end;

    public String roles;
    public int id;
    public int id_organizator;

    public String mapa;

    public List<Submissions> submissions;
    public List<String> committee;

    public String getAddress() {
        return street + ", " + town;
    }

}
