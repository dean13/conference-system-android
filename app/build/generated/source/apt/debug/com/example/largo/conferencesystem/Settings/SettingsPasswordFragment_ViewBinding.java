// Generated code from Butter Knife. Do not modify!
package com.example.largo.conferencesystem.Settings;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.example.largo.conferencesystem.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SettingsPasswordFragment_ViewBinding implements Unbinder {
  private SettingsPasswordFragment target;

  private View view2131755298;

  @UiThread
  public SettingsPasswordFragment_ViewBinding(final SettingsPasswordFragment target, View source) {
    this.target = target;

    View view;
    target.passwordOld = Utils.findRequiredViewAsType(source, R.id.passwordOld, "field 'passwordOld'", EditText.class);
    target.passwordNew = Utils.findRequiredViewAsType(source, R.id.passwordNew, "field 'passwordNew'", EditText.class);
    target.passwordConfirm = Utils.findRequiredViewAsType(source, R.id.passwordConfirm, "field 'passwordConfirm'", EditText.class);
    view = Utils.findRequiredView(source, R.id.fabPassword, "field 'fabPassword' and method 'changePassword'");
    target.fabPassword = Utils.castView(view, R.id.fabPassword, "field 'fabPassword'", FloatingActionButton.class);
    view2131755298 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.changePassword();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SettingsPasswordFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.passwordOld = null;
    target.passwordNew = null;
    target.passwordConfirm = null;
    target.fabPassword = null;

    view2131755298.setOnClickListener(null);
    view2131755298 = null;
  }
}
