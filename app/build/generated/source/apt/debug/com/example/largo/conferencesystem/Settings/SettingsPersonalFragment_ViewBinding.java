// Generated code from Butter Knife. Do not modify!
package com.example.largo.conferencesystem.Settings;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.example.largo.conferencesystem.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SettingsPersonalFragment_ViewBinding implements Unbinder {
  private SettingsPersonalFragment target;

  private View view2131755308;

  @UiThread
  public SettingsPersonalFragment_ViewBinding(final SettingsPersonalFragment target, View source) {
    this.target = target;

    View view;
    target.personalName = Utils.findRequiredViewAsType(source, R.id.personalName, "field 'personalName'", EditText.class);
    target.personalSurname = Utils.findRequiredViewAsType(source, R.id.personalSurname, "field 'personalSurname'", EditText.class);
    target.personalBirth = Utils.findRequiredViewAsType(source, R.id.personalBirth, "field 'personalBirth'", EditText.class);
    target.personalPhone = Utils.findRequiredViewAsType(source, R.id.personalPhone, "field 'personalPhone'", EditText.class);
    target.personalHome = Utils.findRequiredViewAsType(source, R.id.personalHome, "field 'personalHome'", EditText.class);
    target.personalPost = Utils.findRequiredViewAsType(source, R.id.personalPost, "field 'personalPost'", EditText.class);
    target.personalTown = Utils.findRequiredViewAsType(source, R.id.personalTown, "field 'personalTown'", EditText.class);
    target.personalCountry = Utils.findRequiredViewAsType(source, R.id.personalCountry, "field 'personalCountry'", EditText.class);
    view = Utils.findRequiredView(source, R.id.fabPersonal, "field 'fabPersonal' and method 'savePersonal'");
    target.fabPersonal = Utils.castView(view, R.id.fabPersonal, "field 'fabPersonal'", FloatingActionButton.class);
    view2131755308 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.savePersonal();
      }
    });
    target.personalStreet = Utils.findRequiredViewAsType(source, R.id.personalStreet, "field 'personalStreet'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SettingsPersonalFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.personalName = null;
    target.personalSurname = null;
    target.personalBirth = null;
    target.personalPhone = null;
    target.personalHome = null;
    target.personalPost = null;
    target.personalTown = null;
    target.personalCountry = null;
    target.fabPersonal = null;
    target.personalStreet = null;

    view2131755308.setOnClickListener(null);
    view2131755308 = null;
  }
}
