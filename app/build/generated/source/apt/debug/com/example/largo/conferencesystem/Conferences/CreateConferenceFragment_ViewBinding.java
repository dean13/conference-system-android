// Generated code from Butter Knife. Do not modify!
package com.example.largo.conferencesystem.Conferences;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.example.largo.conferencesystem.MultiSelectionSpinner;
import com.example.largo.conferencesystem.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CreateConferenceFragment_ViewBinding implements Unbinder {
  private CreateConferenceFragment target;

  private View view2131755288;

  private View view2131755289;

  @UiThread
  public CreateConferenceFragment_ViewBinding(final CreateConferenceFragment target, View source) {
    this.target = target;

    View view;
    target.title = Utils.findRequiredViewAsType(source, R.id.title, "field 'title'", EditText.class);
    target.description = Utils.findRequiredViewAsType(source, R.id.description, "field 'description'", EditText.class);
    target.website = Utils.findRequiredViewAsType(source, R.id.website, "field 'website'", EditText.class);
    target.countryConference = Utils.findRequiredViewAsType(source, R.id.country_conference, "field 'countryConference'", EditText.class);
    target.city = Utils.findRequiredViewAsType(source, R.id.city, "field 'city'", EditText.class);
    target.code = Utils.findRequiredViewAsType(source, R.id.code, "field 'code'", EditText.class);
    target.street = Utils.findRequiredViewAsType(source, R.id.street, "field 'street'", EditText.class);
    target.categories = Utils.findRequiredViewAsType(source, R.id.categories, "field 'categories'", Spinner.class);
    target.phone = Utils.findRequiredViewAsType(source, R.id.phone, "field 'phone'", EditText.class);
    target.email = Utils.findRequiredViewAsType(source, R.id.email, "field 'email'", EditText.class);
    view = Utils.findRequiredView(source, R.id.start_date, "field 'startDate' and method 'onDateClicked'");
    target.startDate = Utils.castView(view, R.id.start_date, "field 'startDate'", EditText.class);
    view2131755288 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onDateClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.end_date, "field 'endDate' and method 'onDateClicked'");
    target.endDate = Utils.castView(view, R.id.end_date, "field 'endDate'", EditText.class);
    view2131755289 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onDateClicked(p0);
      }
    });
    target.committee = Utils.findRequiredViewAsType(source, R.id.committee, "field 'committee'", MultiSelectionSpinner.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CreateConferenceFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.title = null;
    target.description = null;
    target.website = null;
    target.countryConference = null;
    target.city = null;
    target.code = null;
    target.street = null;
    target.categories = null;
    target.phone = null;
    target.email = null;
    target.startDate = null;
    target.endDate = null;
    target.committee = null;

    view2131755288.setOnClickListener(null);
    view2131755288 = null;
    view2131755289.setOnClickListener(null);
    view2131755289 = null;
  }
}
