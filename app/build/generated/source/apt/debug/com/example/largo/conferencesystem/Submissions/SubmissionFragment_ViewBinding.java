// Generated code from Butter Knife. Do not modify!
package com.example.largo.conferencesystem.Submissions;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.example.largo.conferencesystem.R;
import com.example.largo.conferencesystem.StateRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SubmissionFragment_ViewBinding implements Unbinder {
  private SubmissionFragment target;

  private View view2131755400;

  private View view2131755390;

  @UiThread
  public SubmissionFragment_ViewBinding(final SubmissionFragment target, View source) {
    this.target = target;

    View view;
    target.submissionTitle = Utils.findRequiredViewAsType(source, R.id.submission_title, "field 'submissionTitle'", TextView.class);
    target.submissionAuthor = Utils.findRequiredViewAsType(source, R.id.submission_author, "field 'submissionAuthor'", TextView.class);
    target.submissionOrganization = Utils.findRequiredViewAsType(source, R.id.submission_organization, "field 'submissionOrganization'", TextView.class);
    target.submissionDescription = Utils.findRequiredViewAsType(source, R.id.submission_description, "field 'submissionDescription'", TextView.class);
    view = Utils.findRequiredView(source, R.id.submission_link, "field 'submissionLink' and method 'onDownloadArticle'");
    target.submissionLink = Utils.castView(view, R.id.submission_link, "field 'submissionLink'", Button.class);
    view2131755400 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onDownloadArticle();
      }
    });
    target.opinion = Utils.findRequiredViewAsType(source, R.id.opinion, "field 'opinion'", EditText.class);
    target.rate = Utils.findRequiredViewAsType(source, R.id.rate, "field 'rate'", Spinner.class);
    target.experience = Utils.findRequiredViewAsType(source, R.id.experience, "field 'experience'", Spinner.class);
    view = Utils.findRequiredView(source, R.id.review_save, "field 'reviewSave' and method 'onSaveReview'");
    target.reviewSave = Utils.castView(view, R.id.review_save, "field 'reviewSave'", Button.class);
    view2131755390 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onSaveReview();
      }
    });
    target.allowLayoutReview = Utils.findRequiredViewAsType(source, R.id.allowLayoutReview, "field 'allowLayoutReview'", LinearLayout.class);
    target.deniedInfoReview = Utils.findRequiredViewAsType(source, R.id.deniedInfoReview, "field 'deniedInfoReview'", TextView.class);
    target.deniedLayoutReview = Utils.findRequiredViewAsType(source, R.id.deniedLayoutReview, "field 'deniedLayoutReview'", LinearLayout.class);
    target.reviewRecycler = Utils.findRequiredViewAsType(source, R.id.reviewRecycler, "field 'reviewRecycler'", StateRecyclerView.class);
    target.progressBarReviews = Utils.findRequiredViewAsType(source, R.id.progressBarReviews, "field 'progressBarReviews'", ProgressBar.class);
    target.emptyLayoutReviews = Utils.findRequiredViewAsType(source, R.id.emptyLayoutReviews, "field 'emptyLayoutReviews'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SubmissionFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.submissionTitle = null;
    target.submissionAuthor = null;
    target.submissionOrganization = null;
    target.submissionDescription = null;
    target.submissionLink = null;
    target.opinion = null;
    target.rate = null;
    target.experience = null;
    target.reviewSave = null;
    target.allowLayoutReview = null;
    target.deniedInfoReview = null;
    target.deniedLayoutReview = null;
    target.reviewRecycler = null;
    target.progressBarReviews = null;
    target.emptyLayoutReviews = null;

    view2131755400.setOnClickListener(null);
    view2131755400 = null;
    view2131755390.setOnClickListener(null);
    view2131755390 = null;
  }
}
