// Generated code from Butter Knife. Do not modify!
package com.example.largo.conferencesystem.Conferences;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.largo.conferencesystem.R;
import com.google.android.gms.maps.MapView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ConferenceFragment_ViewBinding implements Unbinder {
  private ConferenceFragment target;

  @UiThread
  public ConferenceFragment_ViewBinding(ConferenceFragment target, View source) {
    this.target = target;

    target.title = Utils.findRequiredViewAsType(source, R.id.titleConference, "field 'title'", TextView.class);
    target.description = Utils.findRequiredViewAsType(source, R.id.descriptionConference, "field 'description'", TextView.class);
    target.organizer = Utils.findRequiredViewAsType(source, R.id.organizer, "field 'organizer'", TextView.class);
    target.phone = Utils.findRequiredViewAsType(source, R.id.phone, "field 'phone'", TextView.class);
    target.email = Utils.findRequiredViewAsType(source, R.id.email, "field 'email'", TextView.class);
    target.start = Utils.findRequiredViewAsType(source, R.id.start, "field 'start'", TextView.class);
    target.end = Utils.findRequiredViewAsType(source, R.id.end, "field 'end'", TextView.class);
    target.address = Utils.findRequiredViewAsType(source, R.id.address, "field 'address'", TextView.class);
    target.website = Utils.findRequiredViewAsType(source, R.id.website, "field 'website'", TextView.class);
    target.categoriesImage = Utils.findRequiredViewAsType(source, R.id.categoriesImage, "field 'categoriesImage'", ImageView.class);
    target.categories = Utils.findRequiredViewAsType(source, R.id.categories, "field 'categories'", TextView.class);
    target.mapView = Utils.findRequiredViewAsType(source, R.id.mapConference, "field 'mapView'", MapView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ConferenceFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.title = null;
    target.description = null;
    target.organizer = null;
    target.phone = null;
    target.email = null;
    target.start = null;
    target.end = null;
    target.address = null;
    target.website = null;
    target.categoriesImage = null;
    target.categories = null;
    target.mapView = null;
  }
}
