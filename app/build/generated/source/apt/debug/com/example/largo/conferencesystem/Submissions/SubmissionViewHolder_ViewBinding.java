// Generated code from Butter Knife. Do not modify!
package com.example.largo.conferencesystem.Submissions;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.example.largo.conferencesystem.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SubmissionViewHolder_ViewBinding implements Unbinder {
  private SubmissionViewHolder target;

  private View view2131755401;

  private View view2131755402;

  @UiThread
  public SubmissionViewHolder_ViewBinding(final SubmissionViewHolder target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.submissionTitle, "field 'submissionTitle' and method 'onSubmissionTitle'");
    target.submissionTitle = Utils.castView(view, R.id.submissionTitle, "field 'submissionTitle'", TextView.class);
    view2131755401 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onSubmissionTitle();
      }
    });
    target.submissionAuthor = Utils.findRequiredViewAsType(source, R.id.submissionAuthor, "field 'submissionAuthor'", TextView.class);
    target.submissionDeadline = Utils.findRequiredViewAsType(source, R.id.submissionDeadline, "field 'submissionDeadline'", TextView.class);
    view = Utils.findRequiredView(source, R.id.submissionAction, "field 'submissionAction' and method 'onSubmissionAction'");
    target.submissionAction = Utils.castView(view, R.id.submissionAction, "field 'submissionAction'", ImageButton.class);
    view2131755402 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onSubmissionAction();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SubmissionViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.submissionTitle = null;
    target.submissionAuthor = null;
    target.submissionDeadline = null;
    target.submissionAction = null;

    view2131755401.setOnClickListener(null);
    view2131755401 = null;
    view2131755402.setOnClickListener(null);
    view2131755402 = null;
  }
}
