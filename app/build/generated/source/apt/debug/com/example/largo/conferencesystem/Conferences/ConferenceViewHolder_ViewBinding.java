// Generated code from Butter Knife. Do not modify!
package com.example.largo.conferencesystem.Conferences;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.example.largo.conferencesystem.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ConferenceViewHolder_ViewBinding implements Unbinder {
  private ConferenceViewHolder target;

  private View view2131755253;

  @UiThread
  public ConferenceViewHolder_ViewBinding(final ConferenceViewHolder target, View source) {
    this.target = target;

    View view;
    target.conferenceTitle = Utils.findRequiredViewAsType(source, R.id.conferenceTitle, "field 'conferenceTitle'", TextView.class);
    target.conferenceCity = Utils.findRequiredViewAsType(source, R.id.conferenceCity, "field 'conferenceCity'", TextView.class);
    view = Utils.findRequiredView(source, R.id.conferenceGo, "field 'conferenceGo' and method 'goConference'");
    target.conferenceGo = Utils.castView(view, R.id.conferenceGo, "field 'conferenceGo'", ImageButton.class);
    view2131755253 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.goConference();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ConferenceViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.conferenceTitle = null;
    target.conferenceCity = null;
    target.conferenceGo = null;

    view2131755253.setOnClickListener(null);
    view2131755253 = null;
  }
}
