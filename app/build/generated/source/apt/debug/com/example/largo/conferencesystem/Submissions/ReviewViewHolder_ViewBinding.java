// Generated code from Butter Knife. Do not modify!
package com.example.largo.conferencesystem.Submissions;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.largo.conferencesystem.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ReviewViewHolder_ViewBinding implements Unbinder {
  private ReviewViewHolder target;

  @UiThread
  public ReviewViewHolder_ViewBinding(ReviewViewHolder target, View source) {
    this.target = target;

    target.reviewOpinion = Utils.findRequiredViewAsType(source, R.id.review_opinion, "field 'reviewOpinion'", TextView.class);
    target.reviewAuthor = Utils.findRequiredViewAsType(source, R.id.review_author, "field 'reviewAuthor'", TextView.class);
    target.reviewDate = Utils.findRequiredViewAsType(source, R.id.review_date, "field 'reviewDate'", TextView.class);
    target.reviewRate = Utils.findRequiredViewAsType(source, R.id.review_rate, "field 'reviewRate'", TextView.class);
    target.reviewExperience = Utils.findRequiredViewAsType(source, R.id.review_experience, "field 'reviewExperience'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ReviewViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.reviewOpinion = null;
    target.reviewAuthor = null;
    target.reviewDate = null;
    target.reviewRate = null;
    target.reviewExperience = null;
  }
}
