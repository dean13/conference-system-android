// Generated code from Butter Knife. Do not modify!
package com.example.largo.conferencesystem;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RestoreActivity_ViewBinding implements Unbinder {
  private RestoreActivity target;

  private View view2131755215;

  @UiThread
  public RestoreActivity_ViewBinding(RestoreActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public RestoreActivity_ViewBinding(final RestoreActivity target, View source) {
    this.target = target;

    View view;
    target.restoreTextEmail = Utils.findRequiredViewAsType(source, R.id.restore_text_email, "field 'restoreTextEmail'", EditText.class);
    view = Utils.findRequiredView(source, R.id.restore_button, "field 'restoreButton' and method 'restore'");
    target.restoreButton = Utils.castView(view, R.id.restore_button, "field 'restoreButton'", Button.class);
    view2131755215 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.restore();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    RestoreActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.restoreTextEmail = null;
    target.restoreButton = null;

    view2131755215.setOnClickListener(null);
    view2131755215 = null;
  }
}
