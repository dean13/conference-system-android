// Generated code from Butter Knife. Do not modify!
package com.example.largo.conferencesystem.Submissions;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.example.largo.conferencesystem.R;
import com.example.largo.conferencesystem.StateRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SubmissionDashboardFragment_ViewBinding implements Unbinder {
  private SubmissionDashboardFragment target;

  private View view2131755321;

  @UiThread
  public SubmissionDashboardFragment_ViewBinding(final SubmissionDashboardFragment target,
      View source) {
    this.target = target;

    View view;
    target.progressBarSubmission = Utils.findRequiredViewAsType(source, R.id.progressBarSubmission, "field 'progressBarSubmission'", ProgressBar.class);
    target.emptyLayoutSubmission = Utils.findRequiredViewAsType(source, R.id.emptyLayoutSubmission, "field 'emptyLayoutSubmission'", LinearLayout.class);
    target.submissionRecycler = Utils.findRequiredViewAsType(source, R.id.submissionRecycler, "field 'submissionRecycler'", StateRecyclerView.class);
    view = Utils.findRequiredView(source, R.id.fabSubmission, "field 'fabSubmission' and method 'onViewClicked'");
    target.fabSubmission = Utils.castView(view, R.id.fabSubmission, "field 'fabSubmission'", FloatingActionButton.class);
    view2131755321 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SubmissionDashboardFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.progressBarSubmission = null;
    target.emptyLayoutSubmission = null;
    target.submissionRecycler = null;
    target.fabSubmission = null;

    view2131755321.setOnClickListener(null);
    view2131755321 = null;
  }
}
