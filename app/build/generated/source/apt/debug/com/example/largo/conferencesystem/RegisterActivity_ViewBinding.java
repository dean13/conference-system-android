// Generated code from Butter Knife. Do not modify!
package com.example.largo.conferencesystem;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RegisterActivity_ViewBinding implements Unbinder {
  private RegisterActivity target;

  @UiThread
  public RegisterActivity_ViewBinding(RegisterActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public RegisterActivity_ViewBinding(RegisterActivity target, View source) {
    this.target = target;

    target.registerUser = Utils.findRequiredViewAsType(source, R.id.register_user, "field 'registerUser'", EditText.class);
    target.registerPassword = Utils.findRequiredViewAsType(source, R.id.register_password, "field 'registerPassword'", EditText.class);
    target.registerEmail = Utils.findRequiredViewAsType(source, R.id.register_email, "field 'registerEmail'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RegisterActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.registerUser = null;
    target.registerPassword = null;
    target.registerEmail = null;
  }
}
