// Generated code from Butter Knife. Do not modify!
package com.example.largo.conferencesystem.Submissions;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.example.largo.conferencesystem.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SubmissionCreateFragment_ViewBinding implements Unbinder {
  private SubmissionCreateFragment target;

  private View view2131755313;

  private View view2131755315;

  @UiThread
  public SubmissionCreateFragment_ViewBinding(final SubmissionCreateFragment target, View source) {
    this.target = target;

    View view;
    target.titleSubmission = Utils.findRequiredViewAsType(source, R.id.titleSubmission, "field 'titleSubmission'", EditText.class);
    target.noteSubmission = Utils.findRequiredViewAsType(source, R.id.noteSubmission, "field 'noteSubmission'", EditText.class);
    target.authorSubmission = Utils.findRequiredViewAsType(source, R.id.authorSubmission, "field 'authorSubmission'", EditText.class);
    target.organizationSubmission = Utils.findRequiredViewAsType(source, R.id.organizationSubmission, "field 'organizationSubmission'", EditText.class);
    view = Utils.findRequiredView(source, R.id.deadlineSubmission, "field 'deadlineSubmission' and method 'onDeadlineClicked'");
    target.deadlineSubmission = Utils.castView(view, R.id.deadlineSubmission, "field 'deadlineSubmission'", EditText.class);
    view2131755313 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onDeadlineClicked();
      }
    });
    target.fileSubmission = Utils.findRequiredViewAsType(source, R.id.fileSubmission, "field 'fileSubmission'", EditText.class);
    view = Utils.findRequiredView(source, R.id.attachmentSubmission, "field 'attachmentSubmission' and method 'clickedAttachment'");
    target.attachmentSubmission = Utils.castView(view, R.id.attachmentSubmission, "field 'attachmentSubmission'", Button.class);
    view2131755315 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.clickedAttachment();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SubmissionCreateFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleSubmission = null;
    target.noteSubmission = null;
    target.authorSubmission = null;
    target.organizationSubmission = null;
    target.deadlineSubmission = null;
    target.fileSubmission = null;
    target.attachmentSubmission = null;

    view2131755313.setOnClickListener(null);
    view2131755313 = null;
    view2131755315.setOnClickListener(null);
    view2131755315 = null;
  }
}
