// Generated code from Butter Knife. Do not modify!
package com.example.largo.conferencesystem.Conferences;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.example.largo.conferencesystem.R;
import com.example.largo.conferencesystem.StateRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DashboardConferenceFragment_ViewBinding implements Unbinder {
  private DashboardConferenceFragment target;

  private View view2131755294;

  @UiThread
  public DashboardConferenceFragment_ViewBinding(final DashboardConferenceFragment target,
      View source) {
    this.target = target;

    View view;
    target.conferenceRecycler = Utils.findRequiredViewAsType(source, R.id.conferenceRecycler, "field 'conferenceRecycler'", StateRecyclerView.class);
    view = Utils.findRequiredView(source, R.id.fab, "field 'fab' and method 'createConference'");
    target.fab = Utils.castView(view, R.id.fab, "field 'fab'", FloatingActionButton.class);
    view2131755294 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.createConference();
      }
    });
    target.progressBarConference = Utils.findRequiredViewAsType(source, R.id.progressBarConference, "field 'progressBarConference'", ProgressBar.class);
    target.emptyLayoutConference = Utils.findRequiredViewAsType(source, R.id.emptyLayoutConference, "field 'emptyLayoutConference'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DashboardConferenceFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.conferenceRecycler = null;
    target.fab = null;
    target.progressBarConference = null;
    target.emptyLayoutConference = null;

    view2131755294.setOnClickListener(null);
    view2131755294 = null;
  }
}
