// Generated code from Butter Knife. Do not modify!
package com.example.largo.conferencesystem.Submissions;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.largo.conferencesystem.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SubmissionAdapter_ViewBinding implements Unbinder {
  private SubmissionAdapter target;

  @UiThread
  public SubmissionAdapter_ViewBinding(SubmissionAdapter target, View source) {
    this.target = target;

    target.submissionAction = Utils.findRequiredViewAsType(source, R.id.submissionAction, "field 'submissionAction'", ImageButton.class);
    target.submissionTitle = Utils.findRequiredViewAsType(source, R.id.submissionTitle, "field 'submissionTitle'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SubmissionAdapter target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.submissionAction = null;
    target.submissionTitle = null;
  }
}
