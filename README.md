# Conference System Android #

Przeniesienie funkcjonalności aplikacji webowej ConferenceSystem na system Android.

### Co znajduje się w repozytorium ? ###

* Cały kod aplikacji mobilnej

### Wykorzystane technologie ###

* Android
* REST API
* Retrofit
* Butterknife
* Otto